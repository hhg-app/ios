//
//  HomeView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 15.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

struct HomeView : View {
    
    @EnvironmentObject var model: Model
    @ObservedObject var user: User
    
    @State var showingProfileSheet = false
    
    @State var index = 0
    
    var calendarPreviewEntries: [CalendarEntry] {
        Array((model.calendarEntries.filter({
            $0.eventType.rawValue < 8 &&
            $0.confirmed &&
            !$0.deleted &&
            Date().timeIntervalSince1970.isLessThanOrEqualTo(Double($0.endTime))
        }).prefix(5)))
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            scrollView
            headerView
        }.statusBar(hidden: true)
    }
    
    private var headerView: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(Date().dayAndDate)
                    .font(.body)
                    .foregroundColor(.secondary)
                Text("Hello \(user.givenName)")
                    .font(.title)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
            }.padding(.top, 30)
            Spacer()
            profileView
        }.padding()
        .background(Color("LightBlue").opacity(0.1))
        .background(Blur(style: .light))
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
    
    private var profileView: some View {
        Button { self.showingProfileSheet.toggle() } label: {
            Image(systemName: "person.fill")
                .resizable()
                .frame(width: 40, height: 40)
                .clipShape(Circle())
                .shadow(radius: 10)
                .overlay(Circle().stroke(Color.primary.opacity(0.06), lineWidth: 4))
                .padding(.top, 30)
        }.buttonStyle(PlainButtonStyle())
        .sheet(isPresented: $showingProfileSheet, content: {
            ProfileView(user: user)
        })
        
    }
    
    private var balanceView: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("\(user.balance)€")
                    .font(.title)
                    .fontWeight(.bold)
                Text("Current Balance")
            }
            Spacer()
            Image("plus")
                .resizable()
                .frame(width: 50, height: 50)
        }.padding()
        .background(Color.primary.opacity(0.07))
        .frame(width: UIScreen.main.bounds.width - 30)
        .cornerRadius(10)
        .padding(.top, 100)
    }
    
    private var upcomingView: some View {
        VStack {
            HStack(alignment: .bottom) {
                Text("Upcoming")
                    .font(.title)
                    .fontWeight(.bold)
                Spacer()
                viewAllCalendarButton
            }
            .padding(.horizontal)
            calendarPreview
        }
    }
    
    private var newsView: some View {
        VStack {
            HStack(alignment: .bottom) {
                Text("News")
                    .font(.title)
                    .fontWeight(.bold)
                Spacer()
            }
            .padding(.horizontal)
        }
    }
    
    private var calendarPreview: some View {
        TabView(selection: self.$index){
            ForEach(0...(calendarPreviewEntries.count - 1), id: \.self) { index in
                calendarPreviewEntries[index].image
                    .resizable()
                    .tag(index)
                    .overlay(VStack {
                        HStack {
                            Spacer()
                            VStack(alignment: .trailing) {
                                Text(Date(timeIntervalSince1970: TimeInterval(calendarPreviewEntries[index].startTime)).shortDayNumeric)
                                    .font(.title)
                                    .fontWeight(.bold)
                                    .foregroundColor(Color.white)
                                Text(Date(timeIntervalSince1970: TimeInterval(calendarPreviewEntries[index].startTime)).shortDay)
                                    .foregroundColor(Color.white)
                            }
                            .padding(.trailing, 10)
                            .padding(.top, 5)
                        }
                        Spacer()
                        HStack {
                            VStack(alignment: .leading) {
                                Text(calendarPreviewEntries[index].title)
                                    .font(.largeTitle)
                                    .fontWeight(.bold)
                                    .foregroundColor(Color.white)
                                Text("\(Date(timeIntervalSince1970: TimeInterval(calendarPreviewEntries[index].startTime)).time) - \(Date(timeIntervalSince1970: TimeInterval(calendarPreviewEntries[index].endTime)).time)")
                                    .foregroundColor(Color.white.opacity(0.9))
                            }
                            .padding(.leading, 5)
                            .padding(.bottom, 10)
                            Spacer()
                        }.background(Blur(style: .regular).opacity(0.5))
                    })
                    .frame(height: 230)
                    .cornerRadius(15)
                    .padding(.horizontal)
            }
        }
        .frame(height: 230)
        .tabViewStyle(PageTabViewStyle())
        .animation(.default)
    }
    
    private var viewAllCalendarButton: some View {
        Button(action: { }, label: {
            Text("View all")
        }).foregroundColor(Color("Blue"))
    }
    
    private var scrollView: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack {
                balanceView
                Divider()
                    .padding(.top, 10)
                    .padding(.bottom, 10)
                upcomingView
                Divider()
                    .padding(.top, 10)
                    .padding(.bottom, 10)
                newsView
            }
            
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(user: User(id: "pfannenstiel", authenticationToken: "test", givenName: "Richard", familyName: "Pfannenstiel", displayName: "Richard Pfannenstiel", roomNumber: "S003", balance: "53.57", group: .none)).environmentObject(Model(calendarEntries: [CalendarEntry(id: 12, title: "Barabend", description: "Bester Barabend EU West", startTime: 1596923802, endTime: 1596927943, eventType: .bar, createdBy: "Marc", confirmed: true, deleted: false)]))
    }
}
