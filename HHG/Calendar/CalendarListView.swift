//
//  CalendarListView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 16.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import SwiftUI

struct CalendarListView: View {
    var body: some View {
        VStack {
            HStack {
                Spacer()
                VStack(alignment: .trailing) {
                    Text("23")
                        .font(.title)
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    Text("So")
                }
            }
            HStack {
                VStack(alignment: .leading) {
                    Text("Barabend")
                        .font(.largeTitle)
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    Text("19:00 - 23:00")
                }
                Spacer()
            }
        }.padding()
    }
}

struct CalendarListView_Previews: PreviewProvider {
    static var previews: some View {
        CalendarListView()
    }
}
