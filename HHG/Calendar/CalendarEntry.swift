//
//  CalendarEntry.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 16.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

struct CalendarEntry {
    
    var id: Int
    var title: String
    var description: String
    var startTime: Int
    var endTime: Int
    var eventType: CalendarEventType
    var createdBy: String
    var confirmed: Bool
    var deleted: Bool
    var image: Image {
        switch eventType {
        case .bar:
            return Image("bar")
        case .cinema:
            return Image("cinema")
        case .gamenight:
            return Image("gamenight")
        case .tutor:
            return Image("tutor_event")
        case .art:
            return Image("art")
        case .kitchen:
            return Image("cooking")
        default: // TODO: Add images for other EventTypes
            return Image("bar")
        }
    }
}
