//
//  CalendarEventType.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 16.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation

enum CalendarEventType: Int {
    
    case bar = 1
    case cinema = 2
    case tutor = 3
    case sky = 4
    case art = 5
    case gamenight = 6
    case official = 7
    case exclusiveUse = 8
    case privateParty = 9
    case kitchen = 10
    case kitchenAndBar = 11
    case other = 12
}
