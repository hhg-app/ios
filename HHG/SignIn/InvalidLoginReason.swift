//
//  InvalidLoginReason.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 16.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation

enum InvalidLoginReason {
    case invalidInputs
    case wrongCredentials
    case refusedConnection
    case biometricNotEnrolled
    case passwordNotSaved
}
