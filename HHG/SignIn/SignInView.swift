//
//  SignInView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 04.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

import LocalAuthentication

struct SignInView: View {
    
    @EnvironmentObject var model: Model
    @State var biometricName = "Face ID"
    @State var biometricImage = Image(systemName: "faceid")
    
    var authentificationContext = LAContext()
    
    @State private var username = ""
    @State private var password = ""
    
    var authenticationToken = Keychain.shared.get(forKey: "authentication")?["token"]
    
    /// Alerts
    @State var showingInvaldLoginAlert = false
    @State var invalidLoginReason: InvalidLoginReason?
    @State var showingPasswordRecoverAlert = false
    
    /// Labels
    var signInDescriptionLabel = "Please login with your HHG-Intranet-Account!"
    var forgotPasswordLabel = "Forgot Password?"
    var forgotPasswordMessageLabel = "Please visit the Intranet website for instructions to reset your password."
    
    /// URLs
    var resetPasswordURL = "https://www.hochschulhaus-garching.de/forgotPassword.php"
    var sessionTimeout = 10.0
    
    /// Loading animation
    @State var loadingCircle = LottieView(filename: "loading_circle", loop: .loop)
    @State var loading = false
    
    var body: some View {
        VStack(alignment: .center) {
            logo
            lockView
            signInDescription
            usernameTextField
            passwordTextField
            forgotPasswordButton
            signInButton
            signInBiometricButton
        }
        .onAppear {
            var authentificationError: NSError?
            if authentificationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authentificationError) {
                biometricName = authentificationContext.biometryType == .faceID ? "Face ID" : "Touch ID"
                biometricImage = authentificationContext.biometryType == .faceID ? Image(systemName: "faceid") : Image(systemName: "touchid")
            }
        }
        .padding(.leading, 30)
        .padding(.trailing, 30)
    }
    
    private var logo: some View {
        Image("logo")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 70, height: 70)
    }
    
    private var lockView: some View {
        Image(systemName: "lock.fill")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 70, height: 70)
            .padding(.bottom, 20)
            .padding(.top, 20)
    }
    
    private var signInDescription: some View {
        Text(signInDescriptionLabel)
            .font(.footnote)
            .multilineTextAlignment(.center)
            .padding(.bottom, 20)
            .padding(.top, 10)
    }
    
    private var usernameTextField: some View {
        HStack {
            Image(systemName: "person.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 20, height: 20)
                .padding(.bottom, 10)
            VStack {
                TextField("Username", text: $username)
                Divider()
            }.padding()
        }
    }
    
    private var passwordTextField: some View {
        HStack {
            Image(systemName: "lock.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 20, height: 20)
                .padding(.bottom, 10)
            VStack {
                SecureField("Password", text: $password)
                Divider()
            }.padding()
        }
    }
    
    private var forgotPasswordButton: some View {
        Button(action: { self.showingPasswordRecoverAlert = true }) {
            Text(forgotPasswordLabel)
                .foregroundColor(Color("Blue"))
        }
        .padding(.bottom, 40)
        .alert(isPresented: $showingPasswordRecoverAlert) {
            Alert(title: Text(forgotPasswordLabel),
                  message: Text(forgotPasswordMessageLabel),
                  primaryButton: .cancel(Text("Cancel")),
                  secondaryButton: .default(Text("Reset password"), action: {
                    if let url = URL(string: self.resetPasswordURL) {
                        UIApplication.shared.open(url)
                    }
            }))
        }
    }
    
    private var signInButton: some View {
        Button(action: checkSignIn) {
            if self.loading {
                loadingCircle
                    .padding(.all, -10)
                    .frame(width: 25, height: 25)
                    .bigButtonStyle()
            } else {
                Text("Sign In")
                    .font(.system(size: 20))
                    .fontWeight(.bold)
                    .bigButtonStyle()
            }
        }.alert(isPresented: $showingInvaldLoginAlert) {
            alertFunc()
        }
    }
    
    private var signInBiometricButton: some View {
        Button(action: signInBiometrically) {
            HStack(spacing: 35){
                biometricImage
                    .font(.system(size: 26))
                    .foregroundColor(Color("Blue"))
                Text("Login With \(biometricName)")
                    .font(.system(size: 20))
                    .fontWeight(.bold)
                    .foregroundColor(Color("Blue"))
                Spacer(minLength: 0)
            }
            .padding()
            .background(RoundedRectangle(cornerRadius: 8).stroke(Color("Blue"),lineWidth: 1))
        }
        .alert(isPresented: $showingInvaldLoginAlert) {
            alertFunc()
        }
        .padding(.top, 10)
    }
    
    private func alertFunc() -> Alert {
        switch self.invalidLoginReason {
        case .invalidInputs:
            return Alert(title: Text("Both fields required"),
                message: Text("Please fill in both username and password in order to sign in."),
                dismissButton: .default(Text("Got it!")))
        case .wrongCredentials:
            return Alert(title: Text("Invalid login!"),
                message: Text("Please check the username and password."),
                dismissButton: .default(Text("Dismiss")))
        case .refusedConnection:
            return Alert(title: Text("Connection refused"),
                message: Text("Please check your internet connection."),
                dismissButton: .default(Text("Dismiss")))
        case .passwordNotSaved:
            return Alert(title: Text("No login data available"),
                         message: Text("You will be able to use biometric authentification after logging in for the first time."), dismissButton: .default(Text("Dismiss")))
        case .biometricNotEnrolled:
            return Alert(title: Text("Authentification error"),
                         message: Text("Biometric authentification is currently not enabled on this device."),
                         primaryButton: .default(Text("Okay")),
                         secondaryButton: .default(Text("Go to Settings")) {
                            UIApplication.shared.open(URL(string: "App-prefs:root=PASSCODE")!)
                         })
        case .none:
            return Alert(title: Text("Unknown Error"),
                message: Text("An unknown error occured, please try again later."),
                dismissButton: .default(Text("Well, fuck!")))
        }
    }
    
    private func signInBiometrically() {
        guard let username = Keychain.shared.get(forKey: "login")?["username"],
              let password = Keychain.shared.get(forKey: "login")?["password"] else {
            invalidLoginReason = .passwordNotSaved
            showingInvaldLoginAlert = true
            return
        }
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Use biometric authentification to log into your HHG Account"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                (success, authentificationError) in
                
                DispatchQueue.main.async {
                    if success {
                        self.username = username as! String
                        self.password = password as! String
                        checkSignIn()
                    }
                }
            }
        } else {
            invalidLoginReason = .biometricNotEnrolled
            showingInvaldLoginAlert = true
        }
    }
    
    private func checkSignIn() {
        if self.username.isEmpty || self.password.isEmpty {
            invalidLoginReason = .invalidInputs
            showingInvaldLoginAlert = true
            return
        }
        startLoadingAnimation()
        
        ServerRequest.requestLogin(username: self.username, password: self.password) { user, error in
            if let user = user {
                // Load Calendar Entries for the Dashboard
                ServerRequest.requestCalendarEntries(authToken: user.authenticationToken) { calendarEntries in
                    DispatchQueue.main.async {
                        Keychain.shared.set(forKey: "authentication", data: ["token": user.authenticationToken])
                        Keychain.shared.set(forKey: "login", data: ["username": self.username, "password": self.password])
                        model.user = user
                        model.calendarEntries = calendarEntries ?? []
                    }
                }
            } else {
                if let error = error {
                    invalidLoginReason = error
                    showingInvaldLoginAlert = true
                }
            }
            stopLoadingAnimation()
        }
    }
    
    private func startLoadingAnimation() {
        loading = true
        loadingCircle.animationView.play()
    }
    
    private func stopLoadingAnimation() {
        loading = false
        loadingCircle.animationView.stop()
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView().environmentObject(Model())
    }
}
