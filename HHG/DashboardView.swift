//
//  Dashboard.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 07.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

struct DashboardView: View {
    
    @EnvironmentObject var model: Model
    @EnvironmentObject var viewManager: ViewManager
    @ObservedObject var user: User
    
    var body: some View {
        ZStack {
            TabView {
                HomeView(user: user).environmentObject(model).tabItem {
                    Image(systemName: "house.fill")
                    Text("Home")
                }
                ResidentsView(user: user, residents: []).environmentObject(viewManager).tabItem {
                    Image(systemName: "person.2.fill")
                    Text("Residents")
                }
                Text("Calendar Dummy").tabItem {
                    Image(systemName: "calendar")
                    Text("Calendar")
                }
            }.onAppear() {
                UITabBar.appearance().backgroundColor = UIColor(Color("LightBlue").opacity(0.3))
            }
            ZStack {
                if viewManager.enableAnimations {
                    actionSheet
                    .animation(.default)
                } else {
                    actionSheet
                }
            }
        }
    }
    
    var actionSheet: some View {
        
        VStack {
            Spacer()
            VStack(spacing: 15) {
                viewManager.sheetView
            }
            .padding(.bottom, (UIApplication.shared.windows.last?.safeAreaInsets.bottom)! + 10)
            .padding(.horizontal)
            .padding(.top,20)
            .background(Color(UIColor.systemBackground))
            .cornerRadius(25)
            
            .offset(y: viewManager.sheetOffset)
            .gesture(DragGesture()
                .onChanged({ (value) in
                    if value.translation.height > 0 {
                        viewManager.sheetOffset = value.location.y
                    }
                })
                .onEnded({ (value) in
                    if viewManager.sheetOffset > 100 {
                        viewManager.sheetOffset = UIScreen.main.bounds.height
                        UIApplication.shared.dismissKeyboard()
                    }
                    else {
                        viewManager.sheetOffset = 0
                    }
                })
            )
            
        }
        .background((viewManager.sheetOffset <= 100 ? Color(UIColor.label).opacity(0.2) : Color.clear).edgesIgnoringSafeArea(.all)
        .onTapGesture {
            viewManager.sheetOffset = UIScreen.main.bounds.height
            UIApplication.shared.dismissKeyboard()
        })
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView(user: User(id: "pfannenstiel", authenticationToken: "test", givenName: "Richard", familyName: "Pfannenstiel", displayName: "Richard Pfannenstiel", roomNumber: "S003", balance: "13.37", group: .tutor))
    }
}
