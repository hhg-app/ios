//
//  SettingsView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 07.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    var body: some View {
        Text("SettingsView")
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
