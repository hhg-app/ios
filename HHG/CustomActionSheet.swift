//
//  CustomActionSheet.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 13.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

struct CustomSheet: View {
    
    @State var offset : CGFloat = UIScreen.main.bounds.height
    //@State var content: some View
    
    var body: some View {
        ZStack{
            Button(action: { self.offset = 0 }) {
                Text("Action Sheet")
            }
            VStack{
                Spacer()
                VStack(spacing: 15){
                    //content
                    
                }.padding(.bottom, (UIApplication.shared.windows.last?.safeAreaInsets.bottom)! + 10)
                .padding(.horizontal)
                .padding(.top,20)
                .background(Color(UIColor.systemBackground))
                .cornerRadius(25)
                
                .offset(y: self.offset)
                .gesture(DragGesture()
                    .onChanged({ (value) in
                        if value.translation.height > 0 {
                            self.offset = value.location.y
                        }
                    })
                    .onEnded({ (value) in
                        if self.offset > 100 {
                            self.offset = UIScreen.main.bounds.height
                        }
                        else{
                            
                            self.offset = 0
                        }
                    })
                )
                
            }
            .background((self.offset <= 100 ? Color(UIColor.label).opacity(0.3) : Color.clear).edgesIgnoringSafeArea(.all)
            .onTapGesture {
                self.offset = 0
            })
            .edgesIgnoringSafeArea(.bottom)
        }.animation(.default)
    }
}
