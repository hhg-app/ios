//
//  SmallButtonModifier.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 11.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

struct SmallButtonModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundColor(.white)
            .padding(.vertical)
            .frame(width: UIScreen.main.bounds.width - 200)
            .background(
            
                LinearGradient(gradient: .init(colors: [Color("DarkBlue"), Color("LightBlue")]), startPoint: .topLeading, endPoint: .bottomTrailing)
            )
            .cornerRadius(8)
    }
}

extension View {
    func smallButtonStyle() -> some View {
        self.modifier(SmallButtonModifier())
    }
}
