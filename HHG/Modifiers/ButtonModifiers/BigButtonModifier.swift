//
//  ButtonModifier.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 08.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

struct BigButtonModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundColor(.white)
            .padding(.vertical)
            .frame(width: UIScreen.main.bounds.width - 50)
            .background(
            
                LinearGradient(gradient: .init(colors: [Color("DarkBlue"), Color("LightBlue")]), startPoint: .topLeading, endPoint: .bottomTrailing)
            )
            .cornerRadius(8)
    }
}

extension View {
    func bigButtonStyle() -> some View {
        self.modifier(BigButtonModifier())
    }
}
