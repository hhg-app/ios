//
//  Resident.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 12.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation

struct Resident: Identifiable {
    
    var id : String
    var givenName : String
    var familyName : String
    var displayName : String
    var roomNumber: String
    
    func sendMoney(amount: Double) -> Bool {
        return true
    }
}

extension Resident: Comparable {
    
    static func < (lhs: Resident, rhs: Resident) -> Bool {
        lhs.familyName < rhs.familyName
    }
}
