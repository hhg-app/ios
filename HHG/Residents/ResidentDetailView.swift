//
//  ResidentDetailView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 12.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import SwiftUI

struct ResidentDetailView: View {
    
    @ObservedObject var user: User
    var resident: Resident
    
    /// Alerts
    @State var showingComingSoonAlert = false
    
    @State var showingMoneyTransfer = false
    
    var body: some View {
        if showingMoneyTransfer {
            MoneyTransferView(user: user, receiver: resident, isPresented: self.$showingMoneyTransfer)
                .animation(.none)
        } else {
            VStack {
                sendMoneyButton
                parcelButton
                contactButton
            }.animation(.none)
        }
    }
    
    private var sendMoneyButton: some View {
        Button(action: sendMoneyAction, label: {
            HStack {
                Text("Send money")
                    .fontWeight(.bold)
                Spacer()
                Image("money_transfer")
                    .resizable()
                    .frame(width: 30, height: 30)
                    .padding(.leading, -10)
            }
            .padding(.vertical,10)
            .padding(.horizontal,25)
            .background(Color.primary.opacity(0.06))
            .cornerRadius(8)
        })
        .buttonStyle(PlainButtonStyle())
    }
    
    private var contactButton: some View {
        Button(action: contactAction, label: {
            HStack {
                Text("Contact")
                    .fontWeight(.bold)
                Spacer()
                Image("whatsapp")
                    .resizable()
                    .frame(width: 30, height: 30)
                    .padding(.leading, -10)
            }
            .padding(.vertical, 10)
            .padding(.horizontal, 25)
            .background(Color.primary.opacity(0.06))
            .cornerRadius(8)
        })
        .buttonStyle(PlainButtonStyle())
        .alert(isPresented: $showingComingSoonAlert, content: {
            Alert(title: Text("Coming soon"),
                  message: Text("This feature will be available in a future version, stay tuned."), dismissButton: .default(Text("Alright!")))
        })
    }
    
    private var parcelButton: some View {
        Button(action: parcelAction, label: {
            HStack {
                Text("Parcel receipt")
                    .fontWeight(.bold)
                Spacer()
                Image("parcel")
                    .resizable()
                    .frame(width: 30, height: 30)
                    .padding(.leading, -10)
            }
            .padding(.vertical, 10)
            .padding(.horizontal, 25)
            .background(Color.primary.opacity(0.06))
            .cornerRadius(8)
        })
        .buttonStyle(PlainButtonStyle())
        .alert(isPresented: $showingComingSoonAlert, content: {
            Alert(title: Text("Coming soon"),
                  message: Text("This feature will be available in a future version, stay tuned."), dismissButton: .default(Text("Alright!")))
        })
    }
    
    private func sendMoneyAction() {
        self.showingMoneyTransfer = true
    }
    
    private func parcelAction() {
        self.showingComingSoonAlert = true
    }
    
    private func contactAction() {
        self.showingComingSoonAlert = true
    }
}

struct ResidentDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ResidentDetailView(user: User(id: "", authenticationToken: "", givenName: "", familyName: "", displayName: "", roomNumber: "", balance: "", group: .none), resident: Resident(id: "007", givenName: "James", familyName: "Bond", displayName: "James Bond", roomNumber: "N007"))
    }
}
