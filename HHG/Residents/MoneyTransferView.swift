//
//  MoneyTransferView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 12.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import SwiftUI
import UIKit

import Combine
import Foundation

struct MoneyTransferView: View {
    
    @ObservedObject var user: User
    @Environment(\.colorScheme) var colorScheme
    var receiver: Resident
    
    @Binding var isPresented: Bool
    
    @State var comment = ""
    @State var amount = ""
    
    @State var sufficientBalance = false
    @State var showingInvalidAmount = false
    
    /// URL Request properties
    var requestURL = "http://10.151.13.5:8000/sendMoney"
    var sessionTimeout = 10.0
    
    /// Transfer animation
    @State var transferAnimation = LottieView(filename: "money_transfer", loop: .playOnce)
    @State var transferingMoney = false
    
    var body: some View {
        VStack(alignment: .center) {
            backButtonView
            VStack {
                transactionPartnersView
                amountView
                if self.transferingMoney {
                    HStack(alignment: .center) {
                        transferAnimation
                            .frame(width: 150, height: 150)
                    }
                } else {
                    commentTextField
                    payButtonView
                }
            }.padding()
        }.onAppear {
            fetchBalance()
            transferAnimation = LottieView(filename: colorScheme == .dark ? "money_transfer_dark" : "money_transfer")
        }.keyboardAdaptive()
        .alert(isPresented: $showingInvalidAmount, content: {
            Alert(title: Text("Invalid transaction"),
                  message: Text("Please change the amount to not exceed your current balance."), dismissButton: .default(Text("Okay")))
        })
    }
    
    private var backButtonView: some View {
        HStack {
            Button(action: goBack, label: {
                HStack {
                    Image(systemName: "chevron.left")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                    Text("Back")
                        .padding(.leading, -8)
                }
            })
            Spacer()
        }.padding(.bottom, 10)
    }
    
    private var transactionPartnersView: some View {
        HStack {
            profilePicture
            VStack(alignment: .leading) {
                Text(user.givenName)
                Text("\(user.balance)€")
                    .font(.caption)
                    .foregroundColor(.gray)
            }
            Spacer()
            directionArrow
            Spacer()
            profilePicture
            VStack(alignment: .leading) {
                Text(receiver.givenName)
                Text(receiver.familyName)
            }
        }
    }
    
    private var profilePicture: some View {
        Image(systemName: "person.fill")
            .resizable()
            .frame(width: 30, height: 30)
            .clipShape(Circle())
            .shadow(radius: 10)
            .overlay(Circle().stroke(Color.primary.opacity(0.06), lineWidth: 4))
    }
    
    private var directionArrow: some View {
        Image(systemName: "arrow.right.circle.fill")
            .resizable()
            .frame(width: 30, height: 30)
    }
    
    private var payButtonView: some View {
        Button(action: sendMoney) {
            Text("Pay Now")
                .font(.system(size: 20))
                .fontWeight(.bold)
                .foregroundColor(.white)
                .padding(.vertical)
                .frame(width: UIScreen.main.bounds.width - 75)
                .background(LinearGradient(gradient: .init(colors: [sufficientBalance ? Color("DarkBlue") : Color.gray, sufficientBalance ? Color("LightBlue") : Color.secondary]), startPoint: .topLeading, endPoint: .bottomTrailing))
                .cornerRadius(8)
        }.padding(.top, 20)
    }
    
    private var commentTextField: some View {
        HStack {
            Image("moneyTransferComment")
                .resizable()
                .scaledToFit()
                .frame(width: 20, height: 20)
                .padding(.bottom, 10)
            VStack {
                TextField("Add a comment", text: $comment)
                Divider()
            }
        }.padding(.top, 20)
    }
    
    private var amountView: some View {
        VStack {
            Text("\((Double(amount) ?? 0) / 100, specifier: "%.2f")")
                .font(.system(size: 60))
                .fontWeight(.bold)
                .multilineTextAlignment(.center)
                .overlay(
                    TextField("", text: $amount)
                        .keyboardType(.numberPad)
                        .foregroundColor(Color.clear)
                        .accentColor(Color.clear)
                        .frame(width: UIScreen.main.bounds.width, height: 100, alignment: .center)
                        .onReceive(Just(amount)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if newValue != filtered {
                                self.amount = filtered
                            }
                            if newValue.count > 6 {
                                self.amount = String(newValue.dropLast())
                            }
                            if newValue.count < 2 {
                                self.amount = newValue.filter { "123456789".contains($0) }
                            }
                            self.sufficientBalance = Double(newValue) ?? 0 <= Double(user.balance)! * 100 && newValue != ""
                        }
                )
            Text("EUR")
                .fontWeight(.bold)
                .padding(.vertical, 5)
                .padding(.horizontal, 20)
                .background(Color("LightBlue").opacity(0.2))
                .clipShape(Capsule())
        }
    }
    
    private func fetchBalance() {
        ServerRequest.requestBalance(authToken: user.authenticationToken) { balance in
            guard let balance = balance else {
                // Could not get current balance
                self.isPresented = false
                return
            }
            DispatchQueue.main.async {
                user.balance = balance
            }
        }
    }
    
    private func sendMoney() {
        UIApplication.shared.dismissKeyboard()
        if !self.sufficientBalance {
            showingInvalidAmount = true
            return
        }
        ServerRequest.requestMoneyTransfer(authToken: user.authenticationToken,
                                           receiverID: receiver.id,
                                           amount: "\((Double(amount) ?? 0) / 100)",
                                           comment: comment) { success in
            transferingMoney = true
            transferAnimation.animationView.play()
            DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 3) {
                self.isPresented = false
                transferingMoney = false
            }
        }
    }
    
    private func goBack() {
        UIApplication.shared.dismissKeyboard()
        self.isPresented = false
    }
}

struct MoneyTransferView_Previews: PreviewProvider {
    
    @State static var isPresented = true
    
    static var previews: some View {
        MoneyTransferView(user: User(id: "pfannenstiel", authenticationToken: "", givenName: "Richard", familyName: "Pfannenstiel", displayName: "Richard Pfannenstiel", roomNumber: "S003", balance: "13.37", group: .none), receiver: Resident(id: "niconu", givenName: "Nico", familyName: "Nusser", displayName: "Nico Nusser", roomNumber: "S311"), isPresented: $isPresented)
    }
}
