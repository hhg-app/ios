//
//  ResidentsView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 11.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

struct ResidentsView: View {
    
    @Environment(\.colorScheme) var colorScheme
    @EnvironmentObject var viewManager: ViewManager
    
    /// Loading animation
    @State var loadingCircle = LottieView(filename: "loading", loop: .loop)
    @State var loading = false
    
    @State var showingFailedView = false
    
    @ObservedObject var user: User
    @State var residents: [Resident] = []
    
    @State var searchText : String = ""
    
    
    var body: some View {
        NavigationView {
            Group {
                if showingFailedView {
                    failedFetchView
                } else {
                    if self.loading {
                        HStack(alignment: .center) {
                            loadingCircle
                                .frame(width: 100, height: 100)
                        }
                    } else {
                        VStack {
                            ScrollView(.vertical, showsIndicators: false) {
                                //SearchBar(text: $searchText, placeholder: "Search")
                                searchBar
                                VStack(spacing: 20){
                                    ForEach(residents.filter(filterResidents).sorted()) { resident in
                                        ResidentCardView(user: user, resident: resident).environmentObject(viewManager)
                                    }
                                }
                            }.navigationBarTitle(Text("Residents"))
                        }
                    }
                }
            }
        }
        .onAppear {
            loadingCircle = LottieView(filename: colorScheme == .dark ? "loading_dark" : "loading", loop: .loop)
            loadData()
        }
    }
    
    private var searchBar: some View {
        TextField("Search", text: self.$searchText)
            .padding(.vertical, 7)
            .padding(.horizontal)
            .background(Color.black.opacity(0.07))
            .cornerRadius(10)
            .padding(.horizontal)
    }
    
    private var residentsList: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 20){
                ForEach(residents) { resident in
                    ResidentCardView(user: user, resident: resident).environmentObject(viewManager)
                }
            }
        }.dismissKeyBoardOnTouch()
    }
    
    private var failedFetchView: some View {
        HStack(alignment: .center) {
            VStack {
                Text("Failed to fetch residents data")
                    .font(.subheadline)
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 20)
                    .padding(.top, 10)
                Button(action: loadData) {
                    Text("Try again")
                        .font(.system(size: 20))
                        .fontWeight(.bold)
                        .smallButtonStyle()
                }
            }
        }
    }
    
    private func filterResidents(resident: Resident) -> Bool {
        return resident.familyName.lowercased().hasPrefix(self.searchText.lowercased()) ||
            resident.givenName.lowercased().hasPrefix(self.searchText.lowercased()) ||
            resident.roomNumber.lowercased().hasPrefix(self.searchText.lowercased()) ||
            searchText == ""
    }
    
    private func startLoadingAnimation() {
        loading = true
        loadingCircle.animationView.play()
    }
    
    private func stopLoadingAnimation() {
        loading = false
        loadingCircle.animationView.stop()
    }
    
    private func loadData() {
        showingFailedView = false
        startLoadingAnimation()
        ServerRequest.requestResidents(authToken: user.authenticationToken) { residents in
            stopLoadingAnimation()
            guard let residents = residents else {
                showingFailedView = true
                return
            }
            self.residents = residents
        }
    }
}

struct ResidentCardView : View {
    
    @EnvironmentObject var viewManager: ViewManager
    
    @ObservedObject var user: User
    var resident : Resident
    
    @State var illegalDetailSelectionAlert = false
    
    var body: some View {
        
        HStack(alignment: .top, spacing: 20) {
            profilePicture
            nameView
            Spacer(minLength: 0)
            infoButtonView
        }.padding()
        .onTapGesture {
            UIApplication.shared.dismissKeyboard()
        }.alert(isPresented: $illegalDetailSelectionAlert, content: {
            Alert(title: Text("Sorry"),
                  message: Text("You cannot interact with your own account."), dismissButton: .default(Text("Okay")))
        })
    }
    
    private var profilePicture: some View {
        Image(systemName: "person.fill")
            .resizable()
            .frame(width: 50, height: 50)
            .clipShape(Circle())
            .shadow(radius: 10)
            .overlay(Circle().stroke(Color.primary.opacity(0.06), lineWidth: 4))
    }
    
    private var nameView: some View {
        VStack(alignment: .leading, spacing: 6) {
            Text(resident.displayName)
                .fontWeight(.bold)
            Text(resident.roomNumber)
                .font(.caption)
                .foregroundColor(.gray)
        }
    }
    
    private var infoButtonView: some View {
        Button(action: {
            showResidentDetailView()
        }) {
            Text("INFO")
                .fontWeight(.bold)
                .padding(.vertical,10)
                .padding(.horizontal,25)
                .background(Color.primary.opacity(0.06))
                .clipShape(Capsule())
        }
    }
    
    private func showResidentDetailView() {
        UIApplication.shared.dismissKeyboard()
        if resident.id == user.id {
            self.illegalDetailSelectionAlert = true
        } else {
            viewManager.sheetView = AnyView(ResidentDetailView(user: user, resident: resident))
            viewManager.sheetOffset = 0
        }
    }
}

struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }

    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }

}
