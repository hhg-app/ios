//
//  WikiView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 14.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import SwiftUI

struct WikiView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct WikiView_Previews: PreviewProvider {
    static var previews: some View {
        WikiView()
    }
}
