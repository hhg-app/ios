//
//  User.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 06.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import Combine

enum UserGroup {
    case admin
    case barteam
    case gkteam
    case haussprecher
    case tutor
    case none
}

class User: ObservableObject {
    
    @Published var authenticationToken: String
    
    @Published var id: String
    @Published var givenName: String
    @Published var familyName: String
    @Published var displayName: String
    @Published var roomNumber: String
    @Published var balance: String
    @Published var group: UserGroup
    
    init(id: String,
         authenticationToken: String,
         givenName: String,
         familyName: String,
         displayName: String,
         roomNumber: String,
         balance: String,
         group: UserGroup) {
        
        self.id = id
        self.authenticationToken = authenticationToken
        self.givenName = givenName
        self.familyName = familyName
        self.displayName = displayName
        self.roomNumber = roomNumber
        self.balance = balance
        self.group = group
    }
}
