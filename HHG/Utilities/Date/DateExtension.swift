//
//  DateExtension.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 15.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation

extension Date {
    var shortDay: String {
        DateFormatter.shortDay.string(from: self)
    }
    var shortDayNumeric: String {
        DateFormatter.shortDayNumeric.string(from: self)
    }
    var dayAndDate: String {
        DateFormatter.weekdayAndDate.string(from: self)
    }
    var day: String {
        DateFormatter.onlyDay.string(from: self)
    }
    var time: String {
        DateFormatter.onlyTime.string(from: self)
    }
    var shortDate: String {
        DateFormatter.onlyDate.string(from: self)
    }
    var age: Int {
        let ageComponents = Calendar.current.dateComponents([.year], from: self, to: Date())
        guard let age = ageComponents.year else {
            return -1
        }
        return age
    }
    var dayBefore: Date {
        guard let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: noon) else {
            return Date()
        }
        return yesterday
    }
    var dayAfter: Date {
        guard let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: noon) else {
            return Date()
        }
        return tomorrow
    }
    var noon: Date {
        guard let noon = Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self) else {
            return Date()
        }
        return noon
    }
    init(dateString: String?) {
        guard let dateString = dateString else {
            self = Date()
            return
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        guard let date = dateFormatter.date(from: dateString) else {
            self = Date()
            return
        }
        self = date
    }
    
    func daysBefore(amount: Int) -> Date {
        guard let thatDay = Calendar.current.date(byAdding: .day, value: -amount, to: noon) else {
            return Date()
        }
        return thatDay
    }
}
