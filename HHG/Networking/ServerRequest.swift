//
//  ServerRequest.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 14.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation

struct ServerRequest {
    
    static let moneyTransferURL = "http://10.151.13.5:8000/sendMoney"
    static let balanceURL = "http://10.151.13.5:8000/balance"
    static var residentsURL = "http://10.151.13.5:8000/residents"
    static var calendarEntriesURL = "http://10.151.13.5:8000/calendarEntries"
    static var loginURL = "http://10.151.13.5:8000/authenticate"
    static let sessionTimeout = 10.0
    
    static func requestMoneyTransfer(authToken: String, receiverID: String, amount: String, comment: String, completion: @escaping (Bool) -> Void) {
        var request = URLRequest(url: URL(string: ServerRequest.moneyTransferURL)!)
        request.httpBody = "{\n\"authtoken\": \"\(authToken)\",\n\"receiver\": \"\(receiverID)\",\n\"amount\": \"\(amount)\",\n\"comment\": \"\(comment)\"}".data(using: .utf8)
        request.httpMethod = "POST"
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = ServerRequest.sessionTimeout
        let session = URLSession(configuration: sessionConfiguration)

        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data else {
                completion(false)
                return
            }
           
            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
                    completion(false)
                    return
                }
                if json["success"] != nil {
                    completion(true)
                }
            } catch {
                completion(false)
            }
        }
        task.resume()
    }
    
    static func requestBalance(authToken: String, completion: @escaping (String?) -> Void) {
        var request = URLRequest(url: URL(string: ServerRequest.balanceURL)!)
        request.httpBody = "{\n\"authtoken\": \"\(authToken)\"}".data(using: .utf8)
        request.httpMethod = "POST"
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = ServerRequest.sessionTimeout
        let session = URLSession(configuration: sessionConfiguration)

        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data else {
                completion(nil)
                return
            }
           
            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
                    completion(nil)
                    return
                }
                if let balance = json["balance"] as? String {
                    completion(balance)
                }
            } catch {
                completion(nil)
            }
        }
        task.resume()
    }
    
    static func requestResidents(authToken: String, completion: @escaping ([Resident]?) -> Void) {
        var request = URLRequest(url: URL(string: ServerRequest.residentsURL)!)
        request.httpBody = "{\n\"authtoken\": \"\(authToken)\"}".data(using: .utf8)
        request.httpMethod = "POST"
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = ServerRequest.sessionTimeout
        let session = URLSession(configuration: sessionConfiguration)

        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data else {
                completion(nil)
                return
            }
           
            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Array<[String : Any]> else {
                    completion(nil)
                    return
                }
                
                var residents: [Resident] = []
                json.forEach { retrievedResident in
                    if let id = retrievedResident["uid"] as? String,
                    let givenName = retrievedResident["givenName"] as? String,
                    let familyName = retrievedResident["familyName"] as? String,
                    let displayName = retrievedResident["displayName"] as? String,
                    let roomNumber = retrievedResident["roomNumber"] as? String {
                        residents.append(Resident(id: id, givenName: givenName, familyName: familyName, displayName: displayName, roomNumber: roomNumber))
                    }
                }
                completion(residents)
            } catch {
                completion(nil)
            }
        }
        task.resume()
    }
    
    static func requestCalendarEntries(authToken: String, completion: @escaping ([CalendarEntry]?) -> Void) {
        var request = URLRequest(url: URL(string: ServerRequest.calendarEntriesURL)!)
        request.httpBody = "{\n\"authtoken\": \"\(authToken)\"}".data(using: .utf8)
        request.httpMethod = "POST"
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = ServerRequest.sessionTimeout
        let session = URLSession(configuration: sessionConfiguration)

        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data else {
                completion(nil)
                return
            }
           
            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Array<[String : Any]> else {
                    completion(nil)
                    return
                }
                var calendarEntries: [CalendarEntry] = []
                json.forEach { calendarEntry in
                    if let id = calendarEntry["id"] as? Int,
                    let title = calendarEntry["title"] as? String,
                    let description = calendarEntry["description"] as? String,
                    let startTime = calendarEntry["starttime"] as? Int,
                    let endTime = calendarEntry["endtime"] as? Int,
                    let eventTypeValue = calendarEntry["eventType"] as? Int,
                    let createdBy = calendarEntry["createdBy"] as? String,
                    let confirmed = calendarEntry["confirmed"] as? Int,
                    let deleted = calendarEntry["deleted"] as? Int {
                        calendarEntries.append(CalendarEntry(id: id, title: title, description: description, startTime: startTime, endTime: endTime, eventType: CalendarEventType.init(rawValue: eventTypeValue) ?? CalendarEventType.other , createdBy: createdBy, confirmed: Bool(truncating: confirmed as NSNumber), deleted: Bool(truncating: deleted as NSNumber)))
                    }
                }
                completion(calendarEntries.sorted(by: { $0.startTime < $1.startTime }))
            } catch {
                completion(nil)
            }
        }
        task.resume()
    }
    
    static func requestLogin(username: String, password: String, completion: @escaping (User?, InvalidLoginReason?) -> Void) {
        var id: String!
        var authenticationToken: String!
        var givenName: String!
        var familyName: String!
        var displayName: String!
        var roomNumber: String!
        var balance: String!
        var group: UserGroup!
        
        var request = URLRequest(url: URL(string: ServerRequest.loginURL)!)
        request.httpBody = "{\n\"username\": \"\(username)\",\n\"password\": \"\(password)\"}".data(using: .utf8)
        request.httpMethod = "POST"
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = ServerRequest.sessionTimeout
        let session = URLSession(configuration: sessionConfiguration)

        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data else {
                completion(nil, .refusedConnection)
                return
            }
            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    completion(nil, .refusedConnection)
                    return
                }
                if let authtoken = json["authtoken"] as? String {
                    authenticationToken = authtoken
                } else {
                    completion(nil, .wrongCredentials)
                    return
                }
                if let receivedUser = json["user"] as? [String : Any] {
                    id = receivedUser["uid"] as? String
                    givenName = receivedUser["givenName"] as? String
                    familyName = receivedUser["familyName"] as? String
                    displayName = receivedUser["displayName"] as? String
                    roomNumber = receivedUser["roomNumber"] as? String
                    balance = receivedUser["balance"] as? String
                } else {
                    completion(nil, .wrongCredentials)
                }
                if let userGroup = json["userGroups"] as? [String : Any] {
                    if userGroup["isAdmin"] as! Bool {
                        group = UserGroup.admin
                    }
                    if userGroup["isBarteam"] as! Bool {
                        group = UserGroup.barteam
                    }
                    if userGroup["isGKTeam"] as! Bool {
                        group = UserGroup.gkteam
                    }
                    if userGroup["isHaussprecher"] as! Bool {
                        group = UserGroup.haussprecher
                    }
                    if userGroup["isTutor"] as! Bool {
                        group = UserGroup.tutor
                    }
                    group = UserGroup.none
                } else {
                    completion(nil, .wrongCredentials)
                    return
                }
                completion(User(id: id, authenticationToken: authenticationToken, givenName: givenName, familyName: familyName, displayName: displayName, roomNumber: roomNumber, balance: balance, group: group), nil)
            } catch {
                completion(nil, .refusedConnection)
            }
        }
        task.resume()
    }
}
