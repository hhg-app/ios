//
//  Model.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 05.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

class Model: ObservableObject {
    
    @Published var user: User?
    @Published var calendarEntries: [CalendarEntry]
    
    init(calendarEntries: [CalendarEntry]? = nil) {
        self.calendarEntries = calendarEntries ?? []
    }
    
}
