//
//  ContentView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 04.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var model: Model
    
    var body: some View {
        return Group {
            if let user = model.user {
                DashboardView(user: user)
                    .environmentObject(model)
                    .environmentObject(ViewManager())
            } else {
                SignInView().environmentObject(model)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
