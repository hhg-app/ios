//
//  ProfileView.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 17.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import SwiftUI

fileprivate enum Selection {
    case profile
    case settings
}

struct ProfileView: View {
    
    @ObservedObject var user: User
    
    @State var searchText = ""
    @State fileprivate var selection = Selection.profile
    
    @State var showingLogoutAlert = false
    
    var body: some View {
        VStack {
            Topbar(selection: self.$selection).padding(.top)
            
            if selection == .profile {
                profileView
            } else {
                settingsView
            }
        }
    }
    
    private var profileView: some View {
        VStack(spacing: 20) {
            HStack() {
                Button { } label: {
                    Image(systemName: "person.fill")
                        .resizable()
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())
                        .shadow(radius: 10)
                        .overlay(Circle().stroke(Color.primary.opacity(0.06), lineWidth: 4))
                }.buttonStyle(PlainButtonStyle())
                VStack(alignment: .leading) {
                    Text(user.displayName)
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        .font(.title3)
                    Text(user.roomNumber)
                        .font(.footnote)
                }.padding(.leading, 5)
                Spacer()
                Button(action: { showingLogoutAlert = true }, label: {
                    VStack(alignment: .center) {
                        Image("logout")
                            .resizable()
                            .frame(width: 30, height: 30)
                            .padding()
                        Text("Logout")
                            .padding(.top, -20)
                            .font(.caption)
                            .foregroundColor(.secondary)
                        
                    }
                }).buttonStyle(PlainButtonStyle())
            }
            
            Divider()
            
            HStack(spacing: 15) {
                HStack {
                    Image(systemName: "calendar")
                    Text("Calendar").fontWeight(.bold)
                }.padding()
                .frame(width: (UIScreen.main.bounds.width - 45) / 2)
                .background(Color("Blue"))
                .cornerRadius(15)
                
                HStack {
                    Image(systemName: "doc.text.magnifyingglass")
                    Text("Wiki").fontWeight(.bold)
                }.padding()
                .frame(width: (UIScreen.main.bounds.width - 45) / 2)
                .background(Color("DarkBlue"))
                .cornerRadius(15)
                
            }.foregroundColor(.white)
            profileAttributesView
            Spacer()
            
        }.padding()
        .padding(.top)
        .alert(isPresented: $showingLogoutAlert, content: {
            Alert(title: Text("Logout"),
                  message: Text("Do you really want to sign out of your account?"),
                  primaryButton: .cancel(Text("Cancel")),
                  secondaryButton: .destructive(Text("Sign out"), action: {
                    self.signOut()
                  }))
        })
    }
    
    private var profileAttributesView: some View {
        VStack {
            Button(action: { }) {
                HStack(spacing: 15) {
                    Image(systemName: "signature")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .padding(.all, 8)
                    Text("Display Name")
                    Spacer()
                    Image(systemName: "arrow.right").renderingMode(.original)
                        .padding(.trailing, 5)
                }
                .padding(.vertical, 5)
                .background(Color("Blue").opacity(0.2))
                .foregroundColor(.black)
                
            }.cornerRadius(10)
            .buttonStyle(PlainButtonStyle())
            
            Button(action: { }) {
                HStack(spacing: 15) {
                    Image("keys")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .padding(.all, 8)
                    Text("Password")
                    Spacer()
                    Image(systemName: "arrow.right").renderingMode(.original)
                        .padding(.trailing, 5)
                }
                .padding(.vertical, 5)
                .background(Color("Blue").opacity(0.2))
                .foregroundColor(.black)
                
            }.cornerRadius(10)
            .buttonStyle(PlainButtonStyle())
            
            Button(action: { }) {
                HStack(spacing: 15) {
                    Image("mail")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .padding(.all, 8)
                    Text("Email")
                    Spacer()
                    Image(systemName: "arrow.right").renderingMode(.original)
                        .padding(.trailing, 5)
                }
                .padding(.vertical, 5)
                .background(Color("Blue").opacity(0.2))
                .foregroundColor(.black)
                
            }.cornerRadius(10)
            .buttonStyle(PlainButtonStyle())
            
            Button(action: { }) {
                HStack(spacing: 15) {
                    Image("outbound")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .padding(.all, 8)
                    Text("Phone")
                    Spacer()
                    Image(systemName: "arrow.right").renderingMode(.original)
                        .padding(.trailing, 5)
                }
                .padding(.vertical, 5)
                .background(Color("Blue").opacity(0.2))
                .foregroundColor(.black)
                
            }.cornerRadius(10)
            .buttonStyle(PlainButtonStyle())
            
            Button(action: { }) {
                HStack(spacing: 15) {
                    Image("gift")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .padding(.all, 8)
                    Text("Birthday")
                    Spacer()
                    Image(systemName: "arrow.right").renderingMode(.original)
                        .padding(.trailing, 5)
                }
                .padding(.vertical, 5)
                .background(Color("Blue").opacity(0.2))
                .foregroundColor(.black)
                
            }.cornerRadius(10)
            .buttonStyle(PlainButtonStyle())
        }
    }
    
    private var settingsView: some View {
        Home()
    }
    
    private func signOut() {
        // TODO: Remove token from Keychain and reset user.
        print("Signing out")
    }
}
  
struct Topbar : View {
    
    @Binding fileprivate var selection: Selection
    
    var body: some View {
        HStack {
            Button(action: { self.selection = .profile }) {
                  Image(systemName: "person.fill")
                    .resizable()
                    .frame(width: 25, height: 25)
                    .padding(.vertical,12)
                    .padding(.horizontal,30)
                    .background(self.selection == .profile ? Color.white : Color.clear)
                    .clipShape(Capsule())
            }.foregroundColor(self.selection == .profile ? Color("Blue") : .gray)
            
            Button(action: { self.selection = .settings }) {
                Image(systemName: "gear")
                    .resizable()
                    .frame(width: 25, height: 25)
                    .padding(.vertical,12)
                    .padding(.horizontal,30)
                    .background(self.selection == .settings ? Color.white : Color.clear)
                    .clipShape(Capsule())
            }.foregroundColor(self.selection == .settings ? Color("Blue") : .gray)
        }.padding(8)
        .background(Color("Color2"))
        .clipShape(Capsule())
        .animation(.default)
    }
}
  
  struct Home : View {
      
      var body : some View{
          
          ScrollView(.vertical, showsIndicators: false) {
              
              VStack(spacing: 15){
                  
                  ForEach(1...8,id: \.self){i in
                      
                      HStack(spacing: 15){
                          
                          Image("pic").renderingMode(.original)
                          
                          VStack(alignment: .leading, spacing: 12) {
                              
                              Text("Catherine")
                              Text("msg \(i)")
                              
                          }
                          
                          Spacer(minLength: 0)
                          
                      }.padding()
                      .background(Color.white)
                  }
                  
              }.padding()
          }
      }
  }

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(user: User(id: "pfanne", authenticationToken: "psfsefsefes", givenName: "Richard", familyName: "Pfannenstiel", displayName: "Richard Pfannenstiel", roomNumber: "S003", balance: "13.37", group: .none))
    }
}
