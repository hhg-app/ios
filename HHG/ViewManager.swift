//
//  ViewManager.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 13.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import SwiftUI

class ViewManager: ObservableObject {
    
    @Published var showSheet = false
    @Published var sheetOffset = UIScreen.main.bounds.height
    @Published var sheetView: AnyView?
    @Published var enableAnimations = true
    
}
