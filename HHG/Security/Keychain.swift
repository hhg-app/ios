//
//  Keychain.swift
//  HHG
//
//  Created by Richard Pfannenstiel on 06.08.20.
//  Copyright © 2020 Richard Pfannenstiel. All rights reserved.
//

import Foundation
import Locksmith

class Keychain {
    
    static let shared = Keychain()
    
    func set(forKey: String, data: [String : Any]) {
        do {
            try Locksmith.updateData(data: data, forUserAccount: forKey)
        } catch {}
    }
    
    func get(forKey: String) -> [String : Any]? {
        Locksmith.loadDataForUserAccount(userAccount: forKey)
    }
    
    func delete(forKey: String) -> Bool {
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: forKey)
            return true
        } catch {
            return false
        }
    }
}
